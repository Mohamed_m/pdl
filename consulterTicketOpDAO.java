import java.sql.*;

import java.util.*;
import java.util.ArrayList;
import java.util.List;
public class consulterTicketOpDAO {
	
	/**
	 * Paramètres de connexion à la base de données oracle URL, LOGIN et PASS
	 * sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "****";  //exemple BDD1
	final static String PASS = "****";   //exemple BDD1
	private Object ticketDAO;
	
	
	/**
	 * Constructeur de la classe
	 * 
	 */
	public consulterTicketOpDAO() {
		// chargement du pilote de bases de données
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err
					.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}
	
	/**
	 * consulter un ticket
	 */
	public void consulterTciketOpDAO(){
		
		// chargement du pilote de bases de données
				try {
					Class.forName("oracle.jdbc.OracleDriver");
				} catch (ClassNotFoundException e) {
					System.err
							.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
				}

			}
	
	
	/**
	 * premet de récupérer le ticket à partir du nom client, prénom client, lieu et description
	 * @param nomClient : nom client
	 * @param prenomClient : prénom client
	 * @param lieu : lieu
	 * @param description : description
	 * @return le ticket trouvé
	 */
	public Ticket getTicket(String nomClient, String prenomClient, String lieu, String description){
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Ticket retour = null;

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM article WHERE art_nomClient = ?");
			ps.setString(1, nomClient);

			// on exécute la requête
			// rs contient un pointeur situé juste avant la première ligne
			// retournée
			rs = ps.executeQuery();
			// passe à la première (et unique) ligne retournée
			if (rs.next())
				retour = new Ticket(rs.getString("art_nomClient"),
						rs.getString("art_prenomClient"),
						rs.getString("art_lieu"), rs.getString("art_description"));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}
		
	
	

	public static void main(String[] args) throws SQLException {
		
		
		consulterTicketOpDAO  ticketDAO = new consulterTicketOpDAO();
		
		
		// test de la méthode getTicket
		Ticket t2 = ((consulterTicketOpDAO) ticketDAO).getTicket("titi","toto","rouen","test");
		System.out.println(t2);

	}
}


