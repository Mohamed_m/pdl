package Interfaces;

import java.awt.FlowLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.Container;

/**
 * Fichier : MenuOperateurGUI.java
 * Classe : MenuOperateurGUI
 * @author mohamed
 */

public class MenuOperateurGUI extends JFrame implements ActionListener {
	
	
	/**
	 * conteneur : il accueille les différents composants graphique de
	 * MenuOperateurGUI
	 */
	private Container container;
	
	/**
	 * bouton consulter un ticket et instantiation
	 */
	private JButton b1 = new JButton ("Consulter un ticket");
	
	/**
	 * bouton traiter un ticket et instantiation
	 */
	private JButton b2 = new JButton ("Traiter un ticket");
	
	/**
	 * bouton créer un compte rendu et instantiation
	 */
	private JButton b3 = new JButton ("Créer un compte rendu");
	
	/**
	 * bouton valider saisie maintenance et instantiation
	 */
	private JButton b4 = new JButton ("Valider saisie maintenance");
	
	
	/**
	 * Constructeur définit la fenêtre et ses composant - affiche la fenêtre
	 */
	public MenuOperateurGUI(){
		
	// on fixe le titre de la fenêtre
	this.setTitle("Menu opérateur");
	
	// initialisation de la taille de la fenêtre
	this.setSize(400, 400);
	
	// permet de quitter l'application si on ferme la fenêtre
	this.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
	
	// positionne la fenêtre au milieu de l'écran
	this.setLocationRelativeTo(null);
	
	container = this.getContentPane();
	container.setLayout(null);		
	
	// ajout des composant au container
	container.add(b1);
	container.add(b2);
	container.add(b3);
	container.add(b4);
	
	// positionnement des composants
	b1.setBounds(100, 50, 200, 20);
	b2.setBounds(100, 100, 200, 20);
	b3.setBounds(100, 150, 200, 20);
	b4.setBounds(100, 200, 200, 20);
	
	//ajout d'une action sur les boutons
	b1.addActionListener(this);
	b2.addActionListener(this);
	b3.addActionListener(this);
	b4.addActionListener(this);
	
	// ajoute les composant au container
	this.setContentPane(container);
	
	// affichage de la fenêtre
	this.setVisible(true);
	
}

	/**
	 * Gère les actions réalisées sur les boutons
	 */
	public void actionPerformed(ActionEvent e) {
		
		this.dispose();
		
		if (e.getSource() == b1) {
		
			ConsulterTicketOpGUI ac = new ConsulterTicketOpGUI();
	}
		if (e.getSource() == b2) {
			
			TraiterTicketOpGUI ac = new TraiterTicketOpGUI();
	}
	
		if (e.getSource() == b3) {
			
			CreerCompteROpGUI ac = new CreerCompteROpGUI();}

			if (e.getSource() == b4) {
				
				ValiderSaisieMaintenanceOpGUI v = new ValiderSaisieMaintenanceOpGUI();

	}

}


	public static void main(String[] args) {
		MenuOperateurGUI m = new MenuOperateurGUI();

	}

}
