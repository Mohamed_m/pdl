package Interfaces;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.CaretListener;
import java.awt.Container;

/**
 * Fichier : MenuStatutGUI.java
 * Classe : MenuStatutGUI
 * @author mohamed
 */

public class MenuStatutGUI extends JFrame implements ActionListener{
	
	
	// la classe AuthentificationClientGUI a l'attribut f2
	private AuthentificationClientGUI f2;
	
	// la classe AuthentificationOperateurGUI a l'attribut f3
	private AuthentificationOperateurGUI f3;
	
	
	/**
	 * conteneur : il accueille les différents composants graphique de
	 * MenuStatutGUI
	 */
	private Container container;
	
	
	/**
	 * bouton clientet instantiation
	 */
	private JButton b1 = new JButton ("Client");
	
	/**
	 * bouton opérateur et instantiation
	 */
	private JButton b2 = new JButton ("Opérateur");
	
	
	/**
	 * Constructeur définit la fenêtre et ses composant - affiche la fenêtre
	 */
	public MenuStatutGUI(){
		
		// on fixe le titre de la fenêtre
		this.setTitle("Statut utilisateur");
		
		// initialisation de la taille de la fenêtre
		this.setSize(400, 400);
		
		// permet de quitter l'application si on ferme la fenêtre
		this.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
		
		// positionne la fenêtre au milieu de l'écran
		this.setLocationRelativeTo(null);
		
		container = this.getContentPane();
		container.setLayout(null);
		
		// ajout des composant au container
		container.add(b1);
		container.add(b2);
		
		// positionnement des composants
		b1.setBounds(100, 100, 200, 20);
		b2.setBounds(100, 200, 200, 20);
		
		//ajout d'une action sur les boutons
		b1.addActionListener(this);
		b2.addActionListener(this);
		
		// ajoute les composant au container
		this.setContentPane(container);
		
		// affichage de la fenêtre
		this.setVisible(true);
		
	}

	/**
	 * Gère les actions réalisées sur les boutons
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		
		this.dispose();
		
		if (e.getSource() == b1) {
		
			AuthentificationClientGUI f2 = new AuthentificationClientGUI();
		
	}
		if (e.getSource() == b2){
			AuthentificationOperateurGUI f3 = new AuthentificationOperateurGUI();
		}
			
		}
	

	public static void main(String[] args) {
		MenuStatutGUI mGui = new MenuStatutGUI();

	}

}
