package Interfaces;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.CaretListener;
import java.awt.Container;

/**
 * Fichier : AuthentificationClientGUI.java
 * Classe : ValiderCompteRenduClGUI
 * @author mohamed
 */

public class ValiderCompteRenduClGUI extends JFrame implements ActionListener {

	
	/**
	 * conteneur : il accueille les différents composants graphique de
	 * AutentificationClientGUI
	 */
	private Container container;
	
	/**
	 * bouton retour et instantiation
	 */
	private JButton b1 = new JButton("Retour");
	
	
	public ValiderCompteRenduClGUI (){
		
		// on fixe le titre de la fenêtre
		this.setTitle("Liste des comptes rendus");
		
		// initialisation de la taille de la fenêtre
		this.setSize(400, 400);
		
		// permet de quitter l'application si on ferme la fenêtre
		this.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
		
		// positionne la fenêtre au milieu de l'écran
		this.setLocationRelativeTo(null);
		
		container = this.getContentPane();
		container.setLayout(null);
		
		// ajout des composant au container
		container.add(b1);
		
		// positionnement des composants
		b1.setBounds(20, 20, 100, 20);
		
		// ajoute les composant au container
		this.setContentPane(container);
		
		// affichage de la fenêtre
		this.setVisible(true);
		
		//ajout d'une action sur les boutons
		b1.addActionListener(this);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		this.dispose();
		
		if (e.getSource() == b1) {
		
			MenuClientGUI ac = new MenuClientGUI();
		}
		
	}

	public static void main(String[] args) {
		ValiderCompteRenduClGUI mGui = new ValiderCompteRenduClGUI();

	}

}
