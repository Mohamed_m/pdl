package Interfaces;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.CaretListener;
import java.awt.Container;

/**
 * Fichier : SinscrireClientGUI.java
 * Classe : SinscrireClientGUI
 * @author mohamed
 */

public class SinscrireClientGUI extends JFrame implements ActionListener{
	
	
	/**
	 * conteneur : il accueille les différents composants graphique de
	 * AutentificationClientGUI
	 */
private Container container;
	
/**
 * bouton de validation et instantiation
 */
	private JButton b1 = new JButton ("Valider");
	
	/**
	 * bouton retour et instantiation
	 */
	private JButton b2 = new JButton ("Retour");
	
	/**
	 * zone de texte pour le champ nom et instantiation
	 */
	private JTextField L1 = new JTextField("");
	
	/**
	 * zone de texte pour le champ siret et instantiation
	 */
	private JTextField L2 = new JTextField("");
	
	/**
	 * zone de texte pour le champ adresse et instantiation
	 */
	private JTextField L3 = new JTextField("");
	
	/**
	 * zone de texte pour le champ apeet instantiation
	 */
	private JTextField L4 = new JTextField("");
	
	
	/**
	 * label nom et instantiation
	 */
	private JLabel nom = new JLabel ("Nom entreprise");
	
	/**
	 * label siret et instantiation
	 */
	private JLabel siret = new JLabel("SIRET");
	
	/**
	 * label adresse et instantiation
	 */
	private JLabel adresse = new JLabel("Adresse");
	
	/**
	 * label code ape et instantiation
	 */
	private JLabel ape = new JLabel("Code APE");
	
	/**
	 * Constructeur définit la fenêtre et ses composant - affiche la fenêtre
	 */
	public SinscrireClientGUI(){
		
		/*on instancie la classe inscriptionClientDAO
		this.inscriptionClientDAO = new inscriptionClientDAO();*/
		
		// on fixe le titre de la fenêtre
		this.setTitle("Inscription client");
		
		// initialisation de la taille de la fenêtre
		this.setSize(400, 400);
		
		// permet de quitter l'application si on ferme la fenêtre
		this.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
		
		// positionne la fenêtre au milieu de l'écran
		this.setLocationRelativeTo(null);
		
		container = this.getContentPane();
		container.setLayout(null);
		
		
		// ajout des composant au container
		container.add(nom);
		container.add(siret);
		container.add(adresse);
		container.add(ape);
		
		// positionnement des composants
		nom.setBounds(50, 50, 100, 20);
		siret.setBounds(50, 100, 100, 20);
		adresse.setBounds(50, 150, 100, 20);
		ape.setBounds(50, 220, 100, 20);
		
		// ajout des composant au container
		container.add(L1);
		container.add(L2);
		container.add(L3);
		container.add(L4);
		
		// positionnement des composants
		L1.setBounds(150, 50, 100, 20);
		L2.setBounds(150, 100, 100, 20);
		L3.setBounds(150, 150, 100, 60);
		L4.setBounds(150, 220, 100, 20);
		
		// ajout des composant au container
		container.add(b1);
		container.add(b2);
		
		// positionnement des composants
		b1.setBounds(220,300 ,100, 20);
		b2.setBounds(100,300 ,100, 20);
		
		// ajoute les composant au container
		this.setContentPane(container);
		
		// affichage de la fenêtre
		this.setVisible(true);
		
		//ajout d'une action sur les boutons
		b1.addActionListener(this);
		b2.addActionListener(this);
		
		
		
		
	}
	
	/**
	 * Gère les actions réalisées sur les boutons
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		
		this.dispose();
		
		if (e.getSource() == b1) {
		
			AuthentificationClientGUI a = new AuthentificationClientGUI();
		
	}
		

		if (e.getSource() == b2) {
		
			AuthentificationClientGUI b = new AuthentificationClientGUI();
		
	}
		
	}



	public static void main(String[] args) {
		SinscrireClientGUI s = new SinscrireClientGUI();

	}

}
