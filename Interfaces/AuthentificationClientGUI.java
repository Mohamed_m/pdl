package Interfaces;


import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.CaretListener;
import java.awt.Container;

/**
 * Fichier : AuthentificationClientGUI.java
 * Classe : AuthentificationClientGUI
 * @author mohamed
 */

public class AuthentificationClientGUI extends JFrame implements ActionListener {

	
	/**
	 * conteneur : il accueille les différents composants graphique de
	 * AutentificationClientGUI
	 */
	private Container container1;
	
	/**
	 * bouton de validation et instantiation
	 */
	private JButton b1 = new JButton ("Valider");
	
	/**
	 * bouton d'inscription et instantiation
	 */
	private JButton b2 = new JButton("S'inscrire");
	
	/**
	 * zone de texte pour le champ Id et instantiation
	 */
	private JTextField L1 = new JTextField("");
	
	/**
	 * zone de texte pour le champ mot de passe et instantiation
	 */
	private JTextField L2 = new JTextField("");
	
	/**
	 * label identifiant et instantiation
	 */
	private JLabel Id = new JLabel ("Identifiant");
	
	/**
	 * label mot de passe et instantiation
	 */
	private JLabel Mdp = new JLabel("Mot de passe");

/**
 * Constructeur définit la fenêtre et ses composant - affiche la fenêtre
 */
public AuthentificationClientGUI(){
	
	/*on instancie la classe autentificationClientDAO
	this.authentificationClientDAO = new authentificationClientDAO();*/
	
	// on fixe le titre de la fenêtre
	this.setTitle("Authentification Client");
	
	// initialisation de la taille de la fenêtre
	this.setSize(400, 400);
	
	// permet de quitter l'application si on ferme la fenêtre
	this.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
	
	// positionne la fenêtre au milieu de l'écran
	this.setLocationRelativeTo(null);
	
	container1 = this.getContentPane();
	container1.setLayout(null);
	

	// ajout des composant au container1
	container1.add(Id);
	container1.add(Mdp);
	container1.add(b1);
	container1.add(b2);
	
	container1.add(L1);
	container1.add(L2);
	
	// positionnement des composants
	Id.setBounds(50,50,100,20);
	Mdp.setBounds(50,150,100,20);
	L1.setBounds(150, 50, 100, 20);
	L2.setBounds(150, 150, 100, 20);
	b1.setBounds(200,220,100, 20);
	b2.setBounds(200, 260, 100, 20);
	
	//ajout d'une action sur les boutons
	b1.addActionListener(this);
	b2.addActionListener(this);
	
	// ajoute les composant au container1
	this.setContentPane(container1);
	
	// affichage de la fenêtre
	this.setVisible(true);
}

/**
 * Gère les actions réalisées sur les boutons
 */
public void actionPerformed(ActionEvent e) {
	
	this.dispose();
	
	if (e.getSource() == b1) {
	
		MenuClientGUI ac = new MenuClientGUI();
	
}
	if (e.getSource() == b2) {
		
		SinscrireClientGUI i = new SinscrireClientGUI();
	
}
}



	public static void main(String[] args) {
		AuthentificationClientGUI cGui = new AuthentificationClientGUI();

	}

}
