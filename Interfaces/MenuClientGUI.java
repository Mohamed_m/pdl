package Interfaces;

import java.awt.FlowLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.Container;
/**
 * Fichier : MenuClientGUI.java
 * Classe : MenuClientGUI
 * @author mohamed
 */

public class MenuClientGUI extends JFrame implements ActionListener {


	/**
	 * conteneur : il accueille les différents composants graphique de
	 * MenuClientClientGUI
	 */
private Container container;
	
/**
 * bouton créer un ticket et instantiation
 */
	private JButton b1 = new JButton ("Créer un ticket");
	
	
	/**
	 * bouton valider un ticket et instantiation
	 */
	private JButton b2 = new JButton ("Valider un ticket");
	
	/**
	 * bouton valider un compte rendu et instantiation
	 */
	private JButton b3 = new JButton ("Valider un compte rendu");
	
	
	//private JButton b4 = new JButton ("Consulter un compte rendu");
	
	
	/**
	 * Constructeur définit la fenêtre et ses composant - affiche la fenêtre
	 */
	public MenuClientGUI(){
		
		// on fixe le titre de la fenêtre
	this.setTitle("Menu client");
	
	// initialisation de la taille de la fenêtre
	this.setSize(400, 400);
	
	// permet de quitter l'application si on ferme la fenêtre
	this.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
	
	// positionne la fenêtre au milieu de l'écran
	this.setLocationRelativeTo(null);
	
	container = this.getContentPane();
	container.setLayout(null);
			
	
	// ajout des composant au container
	container.add(b1);
	container.add(b2);
	container.add(b3);
	
	// positionnement des composants
	b1.setBounds(100, 100, 200, 20);
	b2.setBounds(100, 150, 200, 20);
	b3.setBounds(100, 200, 200, 20);
	
	
	// ajoute les composant au container
	this.setContentPane(container);
	
	// affichage de la fenêtre
	this.setVisible(true);
	
	//ajout d'une action sur les boutons
		b1.addActionListener(this);
		b2.addActionListener(this);
		b3.addActionListener(this);
	
}

	/**
	 * Gère les actions réalisées sur les boutons
	 */
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		this.dispose();
		if (e.getSource() == b1) {
			
			CreerTicketClGUI m = new CreerTicketClGUI();
		
	}
		if (e.getSource() == b2) {
			
			ValiderTicketClGUI ac = new ValiderTicketClGUI();
		
	}
		if (e.getSource() == b3) {
		
			ValiderCompteRenduClGUI ac = new ValiderCompteRenduClGUI();
		
	}
		
	}
	
	

	public static void main(String[] args) {
		MenuClientGUI aGui = new MenuClientGUI();
	}

}
