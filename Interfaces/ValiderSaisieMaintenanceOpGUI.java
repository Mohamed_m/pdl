package Interfaces;

import java.awt.FlowLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.Container;

/**
 * Fichier : ValiderSaisieMaintenanceOpGUI.java
 * Classe : ValiderSaisieMaintenanceOpGUI
 * @author mohamed
 */
public class ValiderSaisieMaintenanceOpGUI extends JFrame implements ActionListener{
	
	/**
	 * conteneur : il accueille les différents composants graphique de
	 * MenuOperateurGUI
	 */
	private Container container;
	
	/**
	 * bouton de retour et instantiation
	 */
	private JButton b1 = new JButton("Retour");
	
	
	
	/**
	 * Constructeur définit la fenêtre et ses composant - affiche la fenêtre
	 */
	public ValiderSaisieMaintenanceOpGUI(){
		
		// on fixe le titre de la fenêtre
		this.setTitle("Valider saisie maintenance");
		
		// initialisation de la taille de la fenêtre
		this.setSize(400, 400);
		
		// permet de quitter l'application si on ferme la fenêtre
		this.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
		
		// positionne la fenêtre au milieu de l'écran
		this.setLocationRelativeTo(null);
		
		container = this.getContentPane();
		container.setLayout(null);

		// ajout des composant au container
		container.add(b1);
		
		// positionnement des composants
		b1.setBounds(20,20, 100, 20);
		
		// ajoute les composant au container
		this.setContentPane(container);
		
		// affichage de la fenêtre
		this.setVisible(true);
		
		//ajout d'une action sur les boutons
		b1.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		this.dispose();
		
		if (e.getSource() == b1) {
		
			MenuOperateurGUI ac = new MenuOperateurGUI ();
		
	}
		
	}


	public static void main(String[] args) {
		ValiderSaisieMaintenanceOpGUI v = new ValiderSaisieMaintenanceOpGUI();

	}

}
