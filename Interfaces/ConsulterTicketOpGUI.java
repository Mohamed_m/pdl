package Interfaces;

import java.awt.FlowLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.Container;

/**
 * Fichier : ConsulterTicketOpGUI.java
 * Classe : ConsulterTicketOpGUI
 * @author mohamed
 */
public class ConsulterTicketOpGUI extends JFrame implements ActionListener {
	

/**
 * conteneur : il accueille les différents composants graphique de
 * ConsulterTicketOpGUI
 */
	private Container container;
	
	/**
	 * bouton de validation
	 */
	private JButton b1 = new JButton ("Valider");
	
	/**
	 * bouton de retour
	 */
	private JButton b2 = new JButton("Retour");
	
	/**
	 * zone de texte pour le champ nom client
	 */
	private JTextField nomClient = new JTextField("");
	
	/**
	 * zone de texte pour le champ prénom client
	 */
	
	private JTextField prenomClient = new JTextField("");
	
	/**
	 * zone de texte pour le champ lieu
	 */
	private JTextField lieu = new JTextField("");
	
	/**
	 * zone de texte pour le champ degre importance
	 */
	private JTextField degreImportance = new JTextField("");
	
	/**
	 * zone de texte pour le champ description
	 */
	private JTextField description = new JTextField("");
	
	/**
	 * label nom du client
	 */
	private JLabel l1 = new JLabel ("Nom du client");
	
	/**
	 * label prénom du client
	 */
	private JLabel l2 = new JLabel ("Prénom du client");
	
	/**
	 * label lieu
	 */
	private JLabel l3 = new JLabel ("Lieu");
	
	/**
	 * label degre importance
	 */
	private JLabel l4 = new JLabel ("Degre d'importance");
	
	/**
	 * label description
	 */
	private JLabel l5 = new JLabel ("Description");
	
	/**
	 * Constructeur définit la fenêtre et ses composant - affiche la fenêtre
	 */
	public ConsulterTicketOpGUI(){
		
		
		/*on instancie la classe consulterTicketOpDAO
		this.consulterTicketOpDAO = new consulterTicketOpDAO();*/
		
		// on fixe le titre de la fenêtre
		this.setTitle("Consulter un ticket");
		
		// initialisation de la taille de la fenêtre
		this.setSize(400, 400);
		
		// permet de quitter l'application si on ferme la fenêtre
		this.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
		
		// positionne la fenêtre au milieu de l'écran
		this.setLocationRelativeTo(null);
		
		container = this.getContentPane();
		container.setLayout(null);
		
		
		// ajout des composant au container
		container.add(l1);
		container.add(l2);
		container.add(l3);
		container.add(l4);
		container.add(l5);
		container.add(b1);
		container.add(b2);
		
		// positionnement des composants
		l1.setBounds(50, 50, 200, 20);
		l2.setBounds (50, 100, 200, 20);
		l3.setBounds(50, 150, 200, 20);
		l4.setBounds(50, 200, 200, 20);
		l5.setBounds(50, 250, 200, 20);
		b1.setBounds(270, 300, 100, 20);
		b2.setBounds(20, 20, 100, 20);
		
		// ajout des composant au container
		container.add(nomClient);
		container.add(prenomClient);
		container.add(lieu);
		container.add(degreImportance);
		container.add(description);
		
		
		// positionnement des composants
		nomClient.setBounds(200, 50, 100, 20);
		prenomClient.setBounds (200, 100, 100, 20);
		lieu.setBounds(200, 150, 100, 20);
		degreImportance.setBounds(200, 200, 100, 20);
		description.setBounds(200, 250, 100, 20);
		
		// ajoute les composant au container1
		this.setContentPane(container);
		
		// affichage de la fenêtre
		this.setVisible(true);
		
		//ajout d'une action sur les boutons
		b2.addActionListener(this);
		b1.addActionListener(this);

		
	}



	/**
	 * Gère les actions réalisées sur les boutons
	 */
	public void actionPerformed(ActionEvent e) {
		
		this.dispose();
		
		if (e.getSource() == b2) {
		
			MenuOperateurGUI ret = new MenuOperateurGUI();
	}
		if (e.getSource() == b1) {
			
			MenuOperateurGUI ret = new MenuOperateurGUI();
	}
}


public static void main(String[] args) {
	ConsulterTicketOpGUI oGui = new ConsulterTicketOpGUI();

}
}