package Interfaces;

import java.awt.FlowLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.Container;

/**
 * Fichier : CreerCompteROpGUI.java
 * Classe : CreerCompteROpGUI
 * @author mohamed
 */
public class CreerCompteROpGUI extends JFrame implements ActionListener {
	
	
	
	/**
	 * conteneur : il accueille les différents composants graphique de
	 * CreerCompteROpGUI
	 */
private Container container;
	

/**
 * bouton de validation et instantiation
 */
	private JButton b1 = new JButton ("Valider");
	
	/**
	 * bouton de retour et instantiation
	 */
		private JButton b2 = new JButton ("Retour");
	
	/**
	 * zone de texte pour le champ description et instantiation
	 */
	private JTextField description = new JTextField("");
	
	/**
	 * zone de texte pour le champ nom client et instantiation
	 */
	private JTextField nomClient = new JTextField("");
	
	/**
	 * zone de texte pour le champ date et instantiation
	 */
	private JTextField date = new JTextField("");
	
	/**
	 * zone de texte pour le champ nom opérateur et instantiation
	 */
	private JTextField nomOperateur = new JTextField("");
	
	
	/**
	 * label description et instantiation
	 */
	private JLabel l1 = new JLabel ("Description");
	
	/**
	 * label nom du client et instantiation
	 */
	private JLabel l2 = new JLabel ("Nom du client");
	
	/**
	 * label date et instantiation
	 */
	private JLabel l3 = new JLabel ("Date");
	
	/**
	 * label nom de l'opérateur et instantiation
	 */
	private JLabel l4 = new JLabel ("Nom de l'opérateur");
	
	/**
	 * Constructeur définit la fenêtre et ses composant - affiche la fenêtre
	 */
		
	public CreerCompteROpGUI(){
		
		/*on instancie la classe creerCompteROpDAO
		this.creerCompteROpDAO = new creerCompteROpDAO();*/
		
		// on fixe le titre de la fenêtre
		this.setTitle("Créer un compte rendu");
		
		// initialisation de la taille de la fenêtre
		this.setSize(400, 400);
		
		// permet de quitter l'application si on ferme la fenêtre
		this.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
		
		// positionne la fenêtre au milieu de l'écran
		this.setLocationRelativeTo(null);
		
		container = this.getContentPane();
		container.setLayout(null);
		
		
		// ajout des composant au container
		container.add(b1);
		container.add(b2);
		container.add(l1);
		container.add(l2);
		container.add(l3);
		container.add(l4);
		
		// positionnement des composants
		b1.setBounds(200,250, 100, 20);
		b2.setBounds(20,20, 100, 20);
		l1.setBounds(50, 50, 200, 20);
		l2.setBounds (50, 100, 200, 20);
		l3.setBounds(50, 150, 200, 20);
		l4.setBounds(50, 200, 200, 20);
		
		// ajout des composant au container
		container.add(description);
		container.add(nomClient);
		container.add(date);
		container.add(nomOperateur);
		
		// positionnement des composants
		description.setBounds(200, 50, 200, 20);
		nomClient.setBounds (200, 100, 200, 20);
		date.setBounds(200, 150, 200, 20);
		nomOperateur.setBounds(200, 200, 200, 20);
		
		// ajoute les composant au container
		this.setContentPane(container);
		
		// affichage de la fenêtre
		this.setVisible(true);
		
		//ajout d'une action sur les boutons
		b1.addActionListener(this);
		b2.addActionListener(this);
}

	/**
	 * Gère les actions réalisées sur les boutons
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		this.dispose();
		
		if (e.getSource() == b1) {
		
			MenuOperateurGUI ac = new MenuOperateurGUI();
	}
		
		if (e.getSource() == b2) {
			
			MenuOperateurGUI ac = new MenuOperateurGUI();
	}
		
	}

	public static void main(String[] args) {
		CreerCompteROpGUI c = new CreerCompteROpGUI();

	}

}
