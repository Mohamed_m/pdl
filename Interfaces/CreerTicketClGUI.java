package Interfaces;

import java.awt.FlowLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.Container;

/**
 * Fichier : ValiderSaisieMaintenanceOpGUI.java
 * Classe : ValiderSaisieMaintenanceOpGUI
 * @author mohamed
 */
public class CreerTicketClGUI extends JFrame implements ActionListener{
	
	/**
	 * conteneur : il accueille les différents composants graphique de
	 * MenuOperateurGUI
	 */
	private Container container;
	
	/**
	 * bouton de retour et instantiation
	 */
	private JButton b1 = new JButton("Valider");
	
	/**
	 * zone de texte pour le champ nom client
	 */
	private JTextField L1 = new JTextField("");
	
	/**
	 * zone de texte pour le champ lieu
	 */
	private JTextField L2 = new JTextField("");
	
	/**
	 * zone de texte pour le champ description
	 */
	private JTextField L3 = new JTextField("");
	
	/**
	 * label nom client
	 */
	private JLabel nomClient = new JLabel ("Nom client");
	
	/**
	 * label lieu
	 */
	private JLabel lieu = new JLabel ("Lieu");
	
	/**
	 * label description
	 */
	private JLabel description = new JLabel ("Description");
	
	/**
	 * Constructeur définit la fenêtre et ses composant - affiche la fenêtre
	 */
	public CreerTicketClGUI(){
		
		// on fixe le titre de la fenêtre
		this.setTitle("Creer un ticket");
		
		// initialisation de la taille de la fenêtre
		this.setSize(400, 400);
		
		// permet de quitter l'application si on ferme la fenêtre
		this.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
		
		// positionne la fenêtre au milieu de l'écran
		this.setLocationRelativeTo(null);
		
		container = this.getContentPane();
		container.setLayout(null);
		
		// ajout des composant au container
		container.add(b1);
		container.add(L1);
		container.add(L2);
		container.add(L3);
		
		container.add(nomClient);
		container.add(lieu);
		container.add(description);
		
		// positionnement des composants
		b1.setBounds(250,270, 100, 20);
		nomClient.setBounds(50,50, 100, 20);
		lieu.setBounds(50,100, 100, 20);
		description.setBounds(50,150, 100, 20);
		L1.setBounds(200,50, 100, 20);
		L2.setBounds(200,100, 100, 20);
		L3.setBounds(200,150, 200, 100);
		
		// ajoute les composant container
		this.setContentPane(container);
		
		// affichage de la fenêtre
		this.setVisible(true);
		
		//ajout d'une action sur les boutons
				b1.addActionListener(this);
		
		
	}

	/**
	 * Gère les actions réalisées sur les boutons
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == b1) {
			
			MenuClientGUI ac = new MenuClientGUI();
		
	}
		
	}


	public static void main(String[] args) {
		CreerTicketClGUI vGui = new CreerTicketClGUI();

	}

}