package Interfaces;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.CaretListener;
import java.awt.Container;

/**
 * Fichier : AuthentificationOperateurGUI.java
 * Classe : AuthentificationOperateurGUI
 * @author mohamed
 */

public class AuthentificationOperateurGUI extends JFrame implements ActionListener{

	
	// la classe MenuOperateurGUI a l'attribut f4
	private MenuOperateurGUI f4;

	
	/**
	 * conteneur : il accueille les différents composants graphique de
	 * AutentificationOperateurGUI
	 */
	private Container container2;

	// bouton de validation
	private JButton Valider = new JButton ("Valider");
	
	/**
	 * zone de texte pour le champ Id
	 */
	private JTextField L1 = new JTextField("");
	
	/**
	 * zone de texte pour le champ Id
	 */
	private JTextField L2 = new JTextField("");
	
	/**
	 * label identifiant
	 */
	private JLabel Id = new JLabel ("Identifiant");
	
	/**
	 * label mot de passe
	 */
	private JLabel Mdp = new JLabel("Mot de passe");

	/**
	 * Constructeur définit la fenêtre et ses composant - affiche la fenêtre
	 */
		
public AuthentificationOperateurGUI(){
	
	/*on instancie la classe authentificationOperateurDAO
	this.authentificationOperateurDAO = new authentificationOperateurDAO();*/
	
	// on fixe le titre de la fenêtre
	this.setTitle("Authentification Opérateur");
	
	// initialisation de la taille de la fenêtre
	this.setSize(400, 400);
	
	// permet de quitter l'application si on ferme la fenêtre
	this.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
	
	// positionne la fenêtre au milieu de l'écran
	this.setLocationRelativeTo(null);
	
	container2 = this.getContentPane();
	container2.setLayout(null);
	
	// ajout des composant au container2
	container2.add(Id);
	container2.add(Mdp);
	container2.add(Valider);
	container2.add(L1);
	container2.add(L2);

	// positionnement des composants

	Id.setBounds(50,50,100,20);
	L1.setBounds(150, 50, 150, 20);
	L2.setBounds(150, 150, 150, 20);
	Mdp.setBounds(50,150,100,20);
	Valider.setBounds(200, 200, 150, 20);
	
	//ajout d'une action sur les boutons
	Valider.addActionListener(this);
	
	// ajoute les composant au container2
	this.setContentPane(container2);
	
	// affichage de la fenêtre
	this.setVisible(true);
	
}



/**
 * Gère les actions réalisées sur les boutons
 */
public void actionPerformed(ActionEvent e) {
	
	this.dispose();
	
	if (e.getSource() == Valider) {
	
		MenuOperateurGUI ac = new MenuOperateurGUI();
	
}
}


public static void main(String[] args) {
	AuthentificationOperateurGUI oGui = new AuthentificationOperateurGUI();

}
}