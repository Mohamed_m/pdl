import java.util.*;

/**
 * Fichier : User.java
 * Classe : User
 * @author mohamed
 */
public class User {
	
	/**
	 * nom
	 */
	protected String nom;
	
	/**
	 * identifiant
	 */
	protected String identifiant;
	
	/**
	 * mail
	 */
	protected String mail;
	
	/**
	 * mot de passe
	 */
	protected String mdp;
	
	
	/**
	 * Constructeur de la classe User
	 * @param nom : nom
	 * @param identifiant : identifiant
	 * @param mail : mail
	 * @param mdp : mot de passe
	 */
	public User (String nom, String identifiant,String mail,  String mdp)
	{
		this.nom=nom;
		this.identifiant=identifiant;
		this.mdp=mdp;
	}
	
	/**
	 * getter de l'attribut nom
	 * @return nom : nom
	 */
	public String getNom ()
	{ 
		return nom;
	}
	
	/**
	 * getter de l'attribut identifiant
	 * @return identifiant : identifiant
	 */
	public String getIdentifiant ()
	{ 
		return identifiant;
	}

	/**
	 * getter de l'attribut mdp
	 * @return mdp : mot de passe
	 */
	public String getMdp ()
	{ 
		return mdp;
	}
	
	/**
	 * affiche le nom
	 * affiche l'identifiant
	 * affiche le mail
	 * affiche le mot de passe
	 */
	public void afficher ()
	{ 
		System.out.println("Nom : "+nom);
		System.out.println("Identifiant : "+identifiant);
		System.out.println("Mail :  "+mail);
		System.out.println("Mot de passe :  "+mdp);
	}
}
