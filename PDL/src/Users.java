import java.util.*;
public class User {
	protected String nom;
	protected String identifiant;
	protected String mail;
	protected String mdp;
	
	public User (String nom, String identifiant,String mail,  String mdp)
	{
		this.nom=nom;
		this.identifiant=identifiant;
		this.mdp=mdp;
	}
	
	public String getNom ()
	{ 
		return nom;
	}
	
	public String getIdentifiant ()
	{ 
		return identifiant;
	}

	public String getMdp ()
	{ 
		return mdp;
	}
	
	public void afficher ()
	{ 
		System.out.println("Nom : "+nom);
		System.out.println("Identifiant : "+identifiant);
		System.out.println("Mail :  "+mail);
		System.out.println("Mot de passe :  "+mdp);
	}
}