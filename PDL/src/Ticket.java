import java.util.*;
public class Ticket {
	private String nomClient;
	private String prenomClient;
	private String lieu;
	private String description;

	public Ticket (String nomClient, String prenomClient, String lieu, String description)
	{
		this.nomClient=nomClient;
		this.prenomClient=prenomClient;
		this.lieu=lieu;
		this.description=description;
	}
	
	// public Ticket () constructeur sans param�tre

	
	public String getNomClient ()
		{return nomClient;}
	
	public String getPrenomClient ()
		{return prenomClient;}
	
	public String getLieu ()
		{return lieu;}
	
	public String getDescription ()
		{return description;}

	public void setNomClient (String nomClient)
		{ this.nomClient=nomClient;}
	
	public void setPrenomClient (String prenomClient)
		{ this.prenomClient=prenomClient;}
	
	public void setLieu (String lieu)
		{ this.lieu=lieu;}
	
	public void setDesignation (String designation)
		{ this.nomClient=nomClient;}

}

