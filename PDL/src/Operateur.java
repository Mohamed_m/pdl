import java.util.ArrayList;
public class Operateur extends User 
{
	private ArrayList<Maintenance> listeMaintenance;
	private int nbrMaintenanceFait;
	private int nbrMaintenance;
	
	public Operateur (String nom, String identifiant, String mail, String mdp, int nbrMaintenanceFait,ArrayList<Maintenance> listeMaintenance)
	
	{	super(nom, identifiant, mail, mdp) ;
		this.nbrMaintenanceFait = nbrMaintenanceFait;
		listeMaintenance = new ArrayList<Maintenance>();
	}
	
	public int getNbrMaintenanceFait ()
		{ return nbrMaintenanceFait ;
		}
	 
	public int getNbrMaintenance ()
		{ return nbrMaintenance ;
		}
	
}
