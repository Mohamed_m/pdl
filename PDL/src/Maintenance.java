import java.util.*;
public class Maintenance {
	private String  nomMaintenance ;
	private String typeDemande ;
	private Client leClient ;
	private Operateur loperateur ;
	private ArrayList <Devis> leDevis = new ArrayList <Devis>();
	
		public Maintenance (String nomMaintenance, String typeDemande, Devis leDevis)
		{
			this.nomMaintenance=nomMaintenance;
			this.typeDemande=typeDemande;
			this.leDevis=leDevis;
			
		}
		public Devis getDevis ()
			{return leDevis;}
		
		public String getNomMaintenance ()
			{return nomMaintenance;}
		
		public Operateur getOperateur ()
			{return loperateur;}
		
		public String TypeDemande ()
			{return typeDemande;}
		
		public void setDevis (Devis leDevis)
			{this.leDevis=leDevis;}
		
		public void setNomMaintenance (String nomMaintenance)
			{this.nomMaintenance=nomMaintenance;}
		
		public void setOperateur (Operateur loperateur)
			{this.loperateur=loperateur;}
		
	/*	public void creerFicheMaintenance ()
			{Scanner sc= new Scanner (System.in);
			sc= date.nextDate();
		int jour ; int mois; int annee; 
			System.out.println( "Entrez la date :" +date);}*/

		
		public void afficher()
			{System.out.println("Nom de la maintenance : "+ nomMaintenance);
			System.out.println("Type demande : "+ typeDemande);
			System.out.println("Devis "+ leDevis);
			}
		
		public Client getLeClient() 
			{return leClient;}
		
		public void setLeClient(Client leClient) 
		{
			this.leClient = leClient;
		}
	

}