public class CompteRendu {
	
	private String description;
	private String nomClient;
	private String date;
	private String nomOperateur;
	
	public CompteRendu(String description, String nomClient, String date, String nomOperateur){

		this.description = description;
		this.nomClient = nomClient;
		this.date = date;
		this.nomOperateur = nomOperateur;
	}
		
		public String getDescription(){
			return description;
		}
		
		public String getnomClient(){
			return nomClient;
		}
		
		public String getDate(){
			return date;
		}
		
		public String getNomOperateur(){
			return nomOperateur;
		}
		
		public void setNomClient(){
			this.nomClient = nomClient;
			
		}
		
	}