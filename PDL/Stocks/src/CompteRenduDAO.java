import java.sql.*;
import java.util.*;
import java.util.ArrayList;
import java.util.List;
public class CompteRenduDAO {
	
	/**
	 * Paramètres de connexion à la base de données oracle URL, LOGIN et PASS
	 * sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "sys";  //exemple BDD1
	final static String PASS = "Azertyui1";   //exemple BDD1
	private Object compterenduDAO;
	
	
	/**
	 * Constructeur de la classe
	 * @return 
	 * 
	 */
	public void CompteRenduDAO(){
		
		// chargement du pilote de bases de données
				try {
					Class.forName("oracle.jdbc.OracleDriver");
				} catch (ClassNotFoundException e) {
					System.err
							.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
				}

			}
	
	public CompteRendu getCompteRendu(String description, String nomClient, String date, String nomOperateur){
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		CompteRendu retour = null;

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM compte_rendu WHERE nom_client = ?");
			ps.setString(1, nom_client);

			// on exécute la requête
			// rs contient un pointeur situé juste avant la première ligne
			// retournée
			rs = ps.executeQuery();
			// passe à la première (et unique) ligne retournée
			if (rs.next())
				retour = new CompteRendu(rs.getString("description"),
						rs.getString("nom_client"),
						rs.getString("date_cr"), rs.getString("nom_operateur"));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}
		
	
	

	public static void main(String[] args) throws SQLException {
		
		
		CompteRenduDAO  compterenduDAO = new CompteRenduDAO();
		
		
		// test de la méthode getCompteRendu
		CompteRendu cr = ((CompteRenduDAO) compterenduDAO).getCompteRendu("Compte-rendu maintenance curative","Charlemagne","19/06/2016","Rouen");
		System.out.println(cr);

	}
}