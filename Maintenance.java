import java.util.*;

/**
 * Fichier : Maintenance.java
 * Classe : Maintenance
 * @author mohamed
 */

public class Maintenance {
	
	/**
	 * nom de la maintenance
	 */
	private String  nomMaintenance ;
	
	/**
	 * type de demande
	 */
	private String typeDemande ;
	
	/**
	 * le client
	 */
	private Client leClient ;
	
	/**
	 * l'opérateur
	 */
	private Operateur loperateur ;
	
	/**
	 * liste des devis
	 */
	private ArrayList <Devis> leDevis = new ArrayList <Devis>();
	
	/**
	 * Constructeur de la classe Maintenance
	 * @param nomMaintenance : nom maintenance
	 * @param typeDemande : type de la demande
	 * @param leDevis : le devis
	 */
		public Maintenance (String nomMaintenance, String typeDemande, Devis leDevis)
		{
			this.nomMaintenance=nomMaintenance;
			this.typeDemande=typeDemande;
			//this.leDevis=leDevis;
			
		}
		/*public Devis getDevis ()
			{return leDevis;}*/
		
		/**
		 * getter de l'attribut nomMaintenance
		 * @return nomMaintenance : nom maintenance
		 */
		public String getNomMaintenance ()
			{return nomMaintenance;}
		
		/**
		 * getter de l'attribut loperateur
		 * @return loperateur : opérateur
		 */
		public Operateur getOperateur ()
			{return loperateur;}
		
		/**
		 * getter de l'attribut typeDemande
		 * @return typeDemande : type de la demande
		 */
		public String TypeDemande ()
			{return typeDemande;}
		
		/*public void setDevis (Devis leDevis)
			{this.leDevis=leDevis;}*/
		
		/**
		 * setter de l'attribut nomMaintenance
		 * @param nomMaintenance : nom maintenance
		 */
		public void setNomMaintenance (String nomMaintenance)
			{this.nomMaintenance=nomMaintenance;}
		
		/**
		 * setter de l'attribut loperateur
		 * @param loperateur : opérateur
		 */
		public void setOperateur (Operateur loperateur)
			{this.loperateur=loperateur;}
		
	/*	public void creerFicheMaintenance ()
			{Scanner sc= new Scanner (System.in);
			sc= date.nextDate();
		int jour ; int mois; int annee; 
			System.out.println( "Entrez la date :" +date);}*/

		/**
		 * affiche le nom de la maintenance
		 * affiche le type de demande
		 * affiche le devis
		 */
		public void afficher()
			{System.out.println("Nom de la maintenance : "+ nomMaintenance);
			System.out.println("Type demande : "+ typeDemande);
			System.out.println("Devis "+ leDevis);
			}
		
		
		/**
		 * getter de l'attribut leClient
		 * @return leClient : le client
		 */
		public Client getLeClient() 
			{return leClient;}
		
		/**
		 * sette de l'attribut leClient
		 * @param leClient : le client
		 */
		public void setLeClient(Client leClient) 
		{
			this.leClient = leClient;
		}
	

}
