import java.util.ArrayList;

/**
 * Fichier : Operateur.java
 * Classe : Operateur
 * @author mohamed
 */
public class Operateur extends User 
{
	
	/**
	 * liste des maintenances
	 */
	private ArrayList<Maintenance> listeMaintenance;
	
	/**
	 * nombre de maintenance fait
	 */
	private int nbrMaintenanceFait;
	
	/**
	 * nombre de maintenance
	 */
	private int nbrMaintenance;
	
	
	/**
	 * Constructeur de la classe Operateur
	 * @param nom : nom de l'opérateur
	 * @param identifiant : identifiant de l'opérateur
	 * @param mail : mail de l'opérateur
	 * @param mdp : mot de passe
	 * @param nbrMaintenanceFait : nom de maitenance fait
	 * @param listeMaintenance : liste maintenances
	 */
	public Operateur (String nom, String identifiant, String mail, String mdp, int nbrMaintenanceFait,ArrayList<Maintenance> listeMaintenance)
	
	{	super(nom, identifiant, mail, mdp) ;
		this.nbrMaintenanceFait = nbrMaintenanceFait;
		listeMaintenance = new ArrayList<Maintenance>();
	}
	
	/**
	 * getter de l'attribut nbrMaintenanceFait
	 * @return nbrMaintenanceFait : nbr maintenance fait
	 */
	public int getNbrMaintenanceFait ()
		{ return nbrMaintenanceFait ;
		}
	 
	/**
	 * getter de l'attribut nbrMaintenance
	 * @return nbrMaintenance : nbr maintenance
	 */
	public int getNbrMaintenance ()
		{ return nbrMaintenance ;
		}
	
	/**
	 * getter de l'attribut nom
	 * @return nom : nom
	 */
	public String getNom ()
	{ 
		return nom;
	}
	
	/**
	 * getter de l'attribut identifiant
	 * @return identifiant : identifiant
	 */
	public String getIdentifiant ()
	{ 
		return identifiant;
	}

	/**
	 * getter de l'attribut mdp
	 * @return mdp : mot de passe
	 */
	public String getMdp ()
	{ 
		return mdp;
	}
	
	/**
	 * affiche le nom
	 * affiche l'identifiant
	 * affiche le mail
	 * affiche le mot de passe
	 */
	public void afficher ()
	{ 
		System.out.println("Nom : "+nom);
		System.out.println("Identifiant : "+identifiant);
		System.out.println("Mail :  "+mail);
		System.out.println("Mot de passe :  "+mdp);
	}
	
}

