import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.*;


public class AuthentificationClientDAO {
	/**
	 * Paramètres de connexion à la base de données oracle URL, LOGIN et PASS
	 * sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "****";  //exemple BDD1
	final static String PASS = "****";   //exemple BDD1

	
	/**
	 * Constructeur de la classe
	 * 
	 */
	public AuthentificationClientDAO() {
		// chargement du pilote de bases de données
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err
					.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}

	/**
	 * Permet de comparer l'authentification saisie à la BDD
	 * @param article
	 *            l'article à ajouter
	 * @return retourne le nombre de lignes ajoutées dans la table
	 */
	
	@SuppressWarnings("null")
	
	public int comparer(Client aut, int m ) {
		
		System.out.println("Authentification d'un client");
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		Client c = null;
				boolean connect = false;
		int retour = 0;

if(m==1){
		// connexion à la base de données
		try {
			
			
			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			System.out.println("Il s'agit d'un client");
			
			// préparation de l'instruction SQL, chaque ? représente une valeur
						// à communiquer dans l'insertion
						// les getters permettent de récupérer les valeurs des attributs
						// souhaités
			ps = con.prepareStatement("SELECT * FROM CLIENT_AUT WHERE ID_CLIENT = ? AND MDP_AUT = ?")
;
			ps.setString(1, aut.getId());
			System.out.println("id" + aut.getId());
			
			ps.setString(2, aut.getMdp());
			System.out.println("ps" + ps);
			System.out.println("id" + aut.getMdp());
			
			ResultSet rst = ps.executeQuery();
			
			if (rst.next()){
				
				if ((rst.getObject("ID_AUT").toString().equals(aut.getId()))&&(rst.getObject("MDP_AUT").toString().equals(aut.getMdp()))){
					System.out.println("il s'agit de l'authentification d'un responsable");
					connect =true;
				}
			}
				
					rst.close();	
		} catch (Exception e) {
			e.printStackTrace();
		
			
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
}	
	
	else
		
		if(m==2){
			System.out.println("initialisation d'un client");
			
			// connexion à la base de données
			try {

				ps = con.prepareStatement(""
						+ "SELECT * FROM AUTHENTIFICATION_AUT "
						+ "WHERE MATRICULE_AUT = (SELECT NUMEMPL_OP FROM OPERATEUR_OP )");
				// on exécute la requète
				// rs contient un pointeur situé juste avant la première ligne
				// retourne
				rs = ps.executeQuery();
				// passe à la première (et unique) ligne retournée
				if (rs.next())
					if ((rs.getObject("ID_AUT").toString().equals(aut.getId()))&&(rs.getObject("MDP_AUT").toString().equals(aut.getMdp()))){
						connect =true;
						System.out.println("il s'agit de l'authentification d'un employe");
					}
						
			} catch (Exception ee) {
				ee.printStackTrace();
			} finally {
				// fermeture du preparedStatement et de la connexion
				try {
					if (rs != null)
						rs.close();
				} catch (Exception ignore) {
				}
				try {
					if (ps != null)
						ps.close();
				} catch (Exception ignore) {
				}
				try {
					if (con != null)
						con.close();
				} catch (Exception ignore) {
				}
			}
			}

	if (connect == true)
		return 1;
	else
		return 0;

		}
	
	// main permettant de tester la classe
	public static void main(String[] args) throws SQLException {

		

	}
}
