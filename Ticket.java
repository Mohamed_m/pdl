import java.util.*;

/**
 * Fichier : Ticket.java
 * Classe : Ticket
 * @author mohamed
 */

public class Ticket {
	
	/**
	 * nom du client
	 */
	private String nomClient;
	
	/**
	 * prénom du client
	 */
	private String prenomClient;
	
	/**
	 * lieu
	 */
	private String lieu;
	
	/**
	 * description
	 */
	private String description;

	
	/**
	 * Constructeur de la classe Ticket
	 * @param nomClient : nom du client
	 * @param prenomClient : prénom du client
	 * @param lieu : lieu
	 * @param description : description
	 */
	public Ticket (String nomClient, String prenomClient, String lieu, String description)
	{
		this.nomClient=nomClient;
		this.prenomClient=prenomClient;
		this.lieu=lieu;
		this.description=description;
	}
	
	// public Ticket () constructeur sans param�tre

	
	/**
	 * getter de l'attribut nomClient
	 * @return nomClient : nom client
	 */
	public String getNomClient ()
		{return nomClient;}
	
	/**
	 * getter de l'attribut prenomClient
	 * @return prenomClient : prénom client
	 */
	public String getPrenomClient ()
		{return prenomClient;}
	
	
	/**
	 * getter de l'attribut lieu
	 * @return lieu : lieu
	 */
	public String getLieu ()
		{return lieu;}
	
	
	/**
	 * getter de l'attribut description
	 * @return description : description
	 */
	public String getDescription ()
		{return description;}

	
	/**
	 * setter de l'attribut nomClient
	 * @param nomClient : nom client
	 */
	public void setNomClient (String nomClient)
		{ this.nomClient=nomClient;}
	
	
	/**
	 * setter de l'attribut prenomClient
	 * @param prenomClient : prénom client
	 */
	public void setPrenomClient (String prenomClient)
		{ this.prenomClient=prenomClient;}
	
	
	/**
	 * setter de l'attribut lieu
	 * @param lieu : lieu
	 */
	public void setLieu (String lieu)
		{ this.lieu=lieu;}
	
	
	/**
	 * setter de l'attribut designation
	 * @param designation : désignation
	 */
	public void setDesignation (String designation)
		{ this.nomClient=nomClient;}

}

