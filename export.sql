--------------------------------------------------------
--  Fichier cr�� - mercredi-mai-24-2017   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table TACHE
--------------------------------------------------------

  CREATE TABLE "CAROLE"."TACHE" 
   (	"DESIGNATION" VARCHAR2(100 BYTE), 
	"PRIX_HT" FLOAT(126), 
	"PRIX_TTC" FLOAT(126), 
	"QUANTITE" NUMBER(*,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table COMPTE_RENDU
--------------------------------------------------------

  CREATE TABLE "CAROLE"."COMPTE_RENDU" 
   (	"DESCRIPTION" VARCHAR2(500 BYTE), 
	"NOM_CLIENT" VARCHAR2(200 BYTE), 
	"DATE_CR" DATE, 
	"NOM_OPERATEUR" VARCHAR2(200 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table DEVIS
--------------------------------------------------------

  CREATE TABLE "CAROLE"."DEVIS" 
   (	"DATE_DEVIS" DATE, 
	"NUMERO" NUMBER(*,0), 
	"PRIX_TOTAL" FLOAT(126)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table OPERATEUR
--------------------------------------------------------

  CREATE TABLE "CAROLE"."OPERATEUR" 
   (	"NBRE_MAINTENANCE_FAIT" NUMBER(*,0), 
	"NBRE_MAINTENANCE" NUMBER(*,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table MAINTENANCE
--------------------------------------------------------

  CREATE TABLE "CAROLE"."MAINTENANCE" 
   (	"NOM_MAINTENANCE" VARCHAR2(100 BYTE), 
	"TYPE_MAINTENANCE" VARCHAR2(20 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table CLIENT
--------------------------------------------------------

  CREATE TABLE "CAROLE"."CLIENT" 
   (	"NOM_ENTREPRISE" VARCHAR2(50 BYTE), 
	"ADRESSE" VARCHAR2(100 BYTE), 
	"SIRET" NUMBER(*,0), 
	"APE" VARCHAR2(20 BYTE), 
	"NBRE_MAINTENANCE" NUMBER(*,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table TICKET
--------------------------------------------------------

  CREATE TABLE "CAROLE"."TICKET" 
   (	"NOM_CLIENT" VARCHAR2(100 BYTE), 
	"PRENOM_CLIENT" VARCHAR2(100 BYTE), 
	"LIEU" VARCHAR2(100 BYTE), 
	"DESCRIPTION" VARCHAR2(500 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table UTILISATEUR
--------------------------------------------------------

  CREATE TABLE "CAROLE"."UTILISATEUR" 
   (	"NOM" VARCHAR2(100 BYTE), 
	"PRENOM" VARCHAR2(100 BYTE), 
	"IDENTIFIANT" VARCHAR2(50 BYTE), 
	"MAIL" VARCHAR2(50 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
REM INSERTING into CAROLE.TACHE
SET DEFINE OFF;
REM INSERTING into CAROLE.COMPTE_RENDU
SET DEFINE OFF;
REM INSERTING into CAROLE.DEVIS
SET DEFINE OFF;
REM INSERTING into CAROLE.OPERATEUR
SET DEFINE OFF;
REM INSERTING into CAROLE.MAINTENANCE
SET DEFINE OFF;
REM INSERTING into CAROLE.CLIENT
SET DEFINE OFF;
REM INSERTING into CAROLE.TICKET
SET DEFINE OFF;
REM INSERTING into CAROLE.UTILISATEUR
SET DEFINE OFF;
--------------------------------------------------------
--  DDL for Index COMPTE_RENDU_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "CAROLE"."COMPTE_RENDU_PK" ON "CAROLE"."COMPTE_RENDU" ("NOM_CLIENT", "DATE_CR") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index MAINTENANCE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "CAROLE"."MAINTENANCE_PK" ON "CAROLE"."MAINTENANCE" ("NOM_MAINTENANCE") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index CLIENT_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "CAROLE"."CLIENT_PK" ON "CAROLE"."CLIENT" ("SIRET", "NBRE_MAINTENANCE") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index TICKET_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "CAROLE"."TICKET_PK" ON "CAROLE"."TICKET" ("NOM_CLIENT") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index UTILISATEUR_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "CAROLE"."UTILISATEUR_PK" ON "CAROLE"."UTILISATEUR" ("IDENTIFIANT") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  Constraints for Table TACHE
--------------------------------------------------------

  ALTER TABLE "CAROLE"."TACHE" MODIFY ("QUANTITE" NOT NULL ENABLE);
  ALTER TABLE "CAROLE"."TACHE" MODIFY ("PRIX_TTC" NOT NULL ENABLE);
  ALTER TABLE "CAROLE"."TACHE" MODIFY ("PRIX_HT" NOT NULL ENABLE);
  ALTER TABLE "CAROLE"."TACHE" MODIFY ("DESIGNATION" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table COMPTE_RENDU
--------------------------------------------------------

  ALTER TABLE "CAROLE"."COMPTE_RENDU" ADD CONSTRAINT "COMPTE_RENDU_PK" PRIMARY KEY ("NOM_CLIENT", "DATE_CR")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "CAROLE"."COMPTE_RENDU" MODIFY ("NOM_OPERATEUR" NOT NULL ENABLE);
  ALTER TABLE "CAROLE"."COMPTE_RENDU" MODIFY ("DATE_CR" NOT NULL ENABLE);
  ALTER TABLE "CAROLE"."COMPTE_RENDU" MODIFY ("NOM_CLIENT" NOT NULL ENABLE);
  ALTER TABLE "CAROLE"."COMPTE_RENDU" MODIFY ("DESCRIPTION" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table DEVIS
--------------------------------------------------------

  ALTER TABLE "CAROLE"."DEVIS" MODIFY ("PRIX_TOTAL" NOT NULL ENABLE);
  ALTER TABLE "CAROLE"."DEVIS" MODIFY ("NUMERO" NOT NULL ENABLE);
  ALTER TABLE "CAROLE"."DEVIS" MODIFY ("DATE_DEVIS" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table OPERATEUR
--------------------------------------------------------

  ALTER TABLE "CAROLE"."OPERATEUR" MODIFY ("NBRE_MAINTENANCE" NOT NULL ENABLE);
  ALTER TABLE "CAROLE"."OPERATEUR" MODIFY ("NBRE_MAINTENANCE_FAIT" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table MAINTENANCE
--------------------------------------------------------

  ALTER TABLE "CAROLE"."MAINTENANCE" ADD CONSTRAINT "MAINTENANCE_PK" PRIMARY KEY ("NOM_MAINTENANCE")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "CAROLE"."MAINTENANCE" MODIFY ("TYPE_MAINTENANCE" NOT NULL ENABLE);
  ALTER TABLE "CAROLE"."MAINTENANCE" MODIFY ("NOM_MAINTENANCE" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table CLIENT
--------------------------------------------------------

  ALTER TABLE "CAROLE"."CLIENT" ADD CONSTRAINT "CLIENT_PK" PRIMARY KEY ("SIRET", "NBRE_MAINTENANCE")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "CAROLE"."CLIENT" MODIFY ("NBRE_MAINTENANCE" NOT NULL ENABLE);
  ALTER TABLE "CAROLE"."CLIENT" MODIFY ("APE" NOT NULL ENABLE);
  ALTER TABLE "CAROLE"."CLIENT" MODIFY ("SIRET" NOT NULL ENABLE);
  ALTER TABLE "CAROLE"."CLIENT" MODIFY ("ADRESSE" NOT NULL ENABLE);
  ALTER TABLE "CAROLE"."CLIENT" MODIFY ("NOM_ENTREPRISE" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table TICKET
--------------------------------------------------------

  ALTER TABLE "CAROLE"."TICKET" ADD CONSTRAINT "TICKET_PK" PRIMARY KEY ("NOM_CLIENT")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "CAROLE"."TICKET" MODIFY ("DESCRIPTION" NOT NULL ENABLE);
  ALTER TABLE "CAROLE"."TICKET" MODIFY ("LIEU" NOT NULL ENABLE);
  ALTER TABLE "CAROLE"."TICKET" MODIFY ("NOM_CLIENT" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table UTILISATEUR
--------------------------------------------------------

  ALTER TABLE "CAROLE"."UTILISATEUR" ADD CONSTRAINT "UTILISATEUR_PK" PRIMARY KEY ("IDENTIFIANT")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "CAROLE"."UTILISATEUR" MODIFY ("IDENTIFIANT" NOT NULL ENABLE);
  ALTER TABLE "CAROLE"."UTILISATEUR" MODIFY ("NOM" NOT NULL ENABLE);
