import java.util.*;

/**
 * Fichier : Client.java
 * Classe : Client
 * @author mohamed
 */

public class Client {
	
	/**
	 * nom de l'entreprise
	 */
	private String nomEntreprise;
	
	/**
	 * adresse de l'entreprise
	 */
	private String adresse;
	
	/**
	 * numéro siret de l'entreprise
	 */
	private int siret;
	
	/**
	 * numéro ape de l'entreprise
	 */
	private String ape;
	
	/**
	 * liste des maintenances
	 */
	private ArrayList<Maintenance> c_listeMaintenance ;
	
	/**
	 * devis
	 */
	private Devis lesDevis;
	
	/**
	 * nombre de maintenance
	 */
	private int nbrMaintenance;
	
	/**
	 * identifiant
	 */
	private String id;
	
	/**
	 * mot de passe
	 */
	private String mdp;
	

	/**
	 * Constructeur de la classe Client
	 * @param nomEntreprise : nom de l'entreprise
	 * @param adresse : adresse de l'entreprise
	 * @param siret : numéro siret de l'entreprise
	 * @param c_listeMaintenance : liste des maintenances
	 * @param id : identifiant
	 * @param mdp : mot de passe
	 */
public Client (String nomEntreprise, String adresse, int siret, ArrayList<Maintenance> c_listeMaintenance, String id, String mdp){
	this.nomEntreprise=nomEntreprise;
	this.adresse=adresse;
	this.siret=siret;
	c_listeMaintenance= new ArrayList<Maintenance>();
	this.mdp=mdp;
	this.id=id;
	
}

/**
 * getter pour l'attribut nomEntreprise
 * @return nomEntreprise
 */
public String getNomEntreprise() {
	return nomEntreprise;
}

/**
 * getter pour l'attribut adresse
 * @return adresse
 */
public String getAdresse() {
	return adresse;
}

/**
 * getter pour l'attribut ape
 * @return ape
 */
public String getApe() {
	return ape;
}

/**
 * getter pour l'attribut siret
 * @return siret
 */
public int getSiret() {
	return siret;
}

/**
 * getter pour l'attribut nbrMaintenance
 * @return nbrMaintenance
 */
public int getNbrMaintenance(){
	return nbrMaintenance ;
	
}

/**
 * getter pour l'attribut id
 * @return id
 */
public String getId(){
	return id;
}

/**
 * getter pour l'attribut mdp
 * @return mdp
 */
public String getMdp(){
	return mdp;
}

/*public float getPrixTotalMaintenances(){
}*/


/**
 * setter pour l'attribut adresse
 * @param adresse : adresse
 */
public void setAdresse(String adresse) {
	this.adresse = adresse;
}

/**
 * permet d'afficher le nom de l'entreprise
 * permet d'afficher l'adresse
 * permet d'afficher le numéro siret
 * permet d'afficher la liste de maintenance
 */

public void afficher(){
	System.out.println("nom entreprise "+ nomEntreprise);
	System.out.println("adress "+ adresse);
	System.out.println("siret "+ siret);
	System.out.println("liste maintenance "+ c_listeMaintenance);
}
}