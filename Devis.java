import java.util.*;

/**
 * Fichier : Devis.java
 * Classe : Devis
 * @author mohamed
 */

public class Devis {
	
	/**
	 * date
	 */
	private String date;
	
	/**
	 * numéro du devis
	 */
	private int numero;
	
	/**
	 * nom du client
	 */
	private Client leClient;
	
	/**
	 * liste des taches
	 */
	private ArrayList<Tache> listeTache;
	
	/**
	 * prix total
	 */
	private float prixTotal;
	
	/**
	 * Constructeur de la classe Devis
	 * @param date : date
	 * @param listeTache : listeTache
	 * @param numero : numéro de devis
	 * @param prixTotal : prix total du devis
	 */
	public Devis (String date, ArrayList<Tache> listeTache, int numero, float prixTotal) {
		
		this.date=date;
		listeTache = new ArrayList<Tache>();
		this.prixTotal=prixTotal;
		this.numero=numero;
		
	}
	
	/**
	 * getter du prix total
	 * @param tache : tache
	 * @return prixtotal
	 */
	public float getPrixTotal(Tache tache){
		for(int i=0;i<listeTache.size();i++)
			prixTotal=listeTache.get(i).getPrixTTC()+ prixTotal;
			return prixTotal;
		}
	
	/**
	 * affiche la date
	 * affiche la liste des taches
	 * affiche le numéro du devis
	 */
	public void afficher(){
	System.out.println("date"+date);
	System.out.println("tache "+ listeTache);
	System.out.println("numero "+numero);}
	
	/**
	 * ajout d'une tache
	 * @param tache : tache
	 */
	public void ajouterTache(Tache tache){
		listeTache.add(tache);
	}
	
}
