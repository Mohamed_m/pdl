import java.util.*;

/**
 * Fichier : Tache.java
 * Classe : Tache
 * @author mohamed
 */

public class Tache {
	
	/**
	 * designation de la tache
	 */
	private String designation;
	
	/**
	 * pric HT
	 */
	private float prixHT;
	
	/**
	 * prix TTC
	 */
	private float prixTTC;
	
	/**
	 * quantité
	 */
	private int quantite;
	
	
	/**
	 * Constructeur de la classe Tache
	 * @param designation : désignation
	 * @param prixHT : prix HT
	 * @param prixTTC : prix TTC
	 * @param quantite : quantité
	 */
	public Tache(String designation, float prixHT, float prixTTC, int quantite)
	{
		this.designation=designation;
		this.prixHT = prixHT;
		this.prixTTC = prixTTC;
		this.quantite = quantite;
	
	}

	
	/**
	 * getter de l'attribut prixHT
	 * @return prixHT : prix HT
	 */
	public float getPrixHT() {
		return prixHT;
	}

	
	/**
	 * getter de l'attribut prixTTC
	 * @return TTC : prix TTC
	 */
	public float getPrixTTC() {
		return prixTTC;
	}
}
	
	

