import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class validerTicketClientDAO {
	
	/**
	 * Paramètres de connexion à la base de données oracle URL, LOGIN et PASS
	 * sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "****";  //exemple BDD1
	final static String PASS = "****";   //exemple BDD1

	
	/**
	 * Constructeur de la classe
	 * 
	 */
	public validerTicketClientDAO() {
		// chargement du pilote de bases de données
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err
					.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}
	
	/**
	 * Permet de récupérer tous les articles stockés dans la table article
	 * 
	 * @return une ArrayList d'Articles
	 */
	public List<Ticket> getListeTciket() {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Ticket> retour = new ArrayList<Ticket>();

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM article");

			// on exécute la requête
			rs = ps.executeQuery();
			// on parcourt les lignes du résultat
			while (rs.next())
				retour.add(new Ticket(rs.getString("art_nomClient"), rs
						.getString("art_prenomClient"), rs
						.getString("art_lieu"), rs.getString("art_description")));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
