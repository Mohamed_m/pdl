import java.util.*;

/**
 * Fichier : CompteRendu.java
 * Classe : CompteRendu
 * @author mohamed
 */

public class CompteRendu {
	
	/**
	 * description du compte rendu
	 */
	private String description;
	
	/**
	 * nom du client
	 */
	private String nomClient;
	
	/**
	 * date
	 */
	private String date;
	
	/**
	 * nom de l'opérateur
	 */
	private String nomOperateur;
	
	
	/**
	 * Constructeur de la classe CompteRendu
	 * @param description : description
	 * @param nomClient : nom du client
	 * @param date : date
	 * @param nomOperateur : nom de l'opérateur
	 */
	public CompteRendu(String description, String nomClient, String date, String nomOperateur){

		this.description = description;
		this.nomClient = nomClient;
		this.date = date;
		this.nomOperateur = nomOperateur;
	}
		
	
	/**
	 * getter de l'attribut description
	 * @return description
	 */
		public String getDescription(){
			return description;
		}
		
		/**
		 * getter de l'attribut nomClient
		 * @return nomClient
		 */
		public String getnomClient(){
			return nomClient;
		}
		
		/**
		 * getter de l'attribut date
		 * @return date
		 */
		public String getDate(){
			return date;
		}
		
		/**
		 * getter de l'attribut nomOperateur
		 * @return nomOperateur
		 */
		public String getNomOperateur(){
			return nomOperateur;
		}
		
		/**
		 * setter de l'attribut nomClient
		 */
		public void setNomClient(){
			this.nomClient = nomClient;
			
		}
		
	}
